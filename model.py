from sklearn.metrics import make_scorer, fbeta_score
from sklearn.model_selection import TimeSeriesSplit, RandomizedSearchCV
import lightgbm as lgb
from sklearn.model_selection import cross_val_score
import pandas as pd
import seaborn as sns
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import mean_absolute_error

sns.set_style("ticks")


class Modeling():
    """
    Modeling class that offers functions to evaluate model and visualise predicted values.
    A wrapper of the customized model or predefined model from scikit-learn
    """

    def __init__(self, model, params={}):
        self.params = params
        self.model = model

    def fit(self, X, y):
        self.model.fit(X, y)

    def predict(self, X):
        y = self.model.predict(X)

        return y

    def evaluation(self,
                   y_train_true,
                   y_train_pred,
                   y_test_true,
                   y_test_pred,
                   method=["mae", "mape"],
                   verbose=False):
        """
        evaluate model by calculating MAE and MAPE by default

        Parameters:
        -----------
        y_train_true : numpy array or pd. Series
            y train true values

        y_train_pred : numpy array or pd. Series
            y train predicted values

        y_test_true : numpy array or pd. Series
            y test true values

        y_test_pred : numpy array or pd. Series
            y test predicted values

        method: list of str
            specify methods of evaluation, by default MAE and MAPE

        verbose: boolean
            True if wanting to print values

        Returns:
        --------
        eval: dict
            dictionary of evaluation metric as key and and score as value
        """

        eval = {}

        for m in method:

            score_train = eval_metric(y_train_true, y_train_pred, method=m)
            score_test = eval_metric(y_test_true, y_test_pred, method=m)

            if verbose:
                print("Train score (%s) : %.2f" % (m, score_train))
                print("Test score (%s) : %.2f" % (m, score_test))
            eval[m] = {"train": score_train, "test": score_test}

        return eval

    def cv(self, X, y, params, n_iter_search=10, n_splits=5):
        """
        time series cross validation to lookup best parameters using random search

        Parameters:
        -----------
        X : pd.DataFrame
            input dataframe

        y : pd.Series
            target values

        n_iter_search : int
            number of iteration. Default value : 10

        n_splits : int
            number of splits. Default value : 5

        Returns:
        --------
        random_search : scikitlearn random search object

        """
        # run randomized search

        scoring = make_scorer(mean_absolute_error, greater_is_better=False)

        tscv = TimeSeriesSplit(n_splits=n_splits)
        random_search = RandomizedSearchCV(self.model,
                                           param_distributions=params,
                                           scoring=scoring,
                                           n_iter=n_iter_search,
                                           cv=tscv,
                                           iid=False)
        random_search.fit(X, y)

        return random_search

    def prediction_visualization(self, time, y, y_pred, ax=None):
        """
        visualize prediction 

        Parameters:
        -----------
        time: pd.Series or numpy array
            timepoint

        y: pd.Series or numpy array
            true y values

        y_pred : pd.Series or numpy array
            predicted y values

        ax : matplotlib axis
            Default value: None

        """

        sns.lineplot(x=time, y=y, ax=ax, color="blue", label="y_true")
        sns.lineplot(x=time, y=y_pred, ax=ax, color="red", label="y_pred")

    def feature_importances(self, ax, features):
        """
        show feature importance of models, only applicable to models that provide 
        this function.

        Parameters:
        ----------
        ax : matplotlib axis

        features :  list of str
            a list of feature names

        Returns:
        --------
        df_sorted : pd.DataFrame
            a dataframe of output 

        """
        importances = self.model.feature_importances_
        df = pd.DataFrame(zip(features, importances),
                          columns=["features", "importances"])
        df_sorted = df.sort_values(by="importances", ascending=False)
        sns.barplot(x="features", y="importances", data=df_sorted, ax=ax)

        return df_sorted

    @staticmethod
    # Utility function to report best scores
    def report(results, n_top=3):
        for i in range(1, n_top + 1):
            candidates = np.flatnonzero(results['rank_test_score'] == i)
            for candidate in candidates:
                print("Model with rank: {0}".format(i))
                print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                    results['mean_test_score'][candidate],
                    results['std_test_score'][candidate]))
                print("Parameters: {0}".format(results['params'][candidate]))
                print("")


def eval_metric(y_true, y_pred, method="mae"):
    """
    Evaluation metrics

    Arguments
    ---------
    y_true : np.array
        true values
    y_pred : np.array
        predicted values

    Returns
    -------
    res : float
        result value

    """
    if method == "mae":
        res = np.absolute(y_true - y_pred).mean()
    elif method == "mape":
        res = 100 * (np.absolute(y_true - y_pred) / y_true).mean()

    return res


def train_test_split_by_date(df, target="Weekly_Sales", train_size=0.8):
    """
    Divide train and test set by date

    Arguments
    ---------
    df : pandas dataframe
        database

    target : str
        targeted variable

    train_size: fraction
        indicate the ratio of train test dataset

    Returns
    -------
    X_train : pd.DataFrame
        input train values

    X_test : pd.DataFrame
        input test values

    y_train : pd.Series
        target train values

    y_test : pd.Series
        target test values 

    """
    # Division by time
    n_train = int(train_size * df["Timepoint"].nunique())
    df_sorted = df.sort_values(by="Timepoint")
    df_train = df_sorted[df_sorted["Timepoint"] < n_train]
    df_test = df_sorted[df_sorted["Timepoint"] >= n_train].reset_index().drop(
        columns=["index"])

    X_col = [x for x in df.columns.tolist() if not x == target]
    X_train = df_train[X_col]
    y_train = df_train[target]
    X_test = df_test[X_col]
    y_test = df_test[target]

    return X_train, X_test, y_train, y_test


def series_to_supervised(data, target, n_in=1, n_out=1, dropnan=False):
    """
    Frame a time series as a supervised learning dataset.
    Arguments:
    ----------
        data: Sequence of observations as a list or NumPy array.
        target: Sequence of targets as a list or NumPy array
        n_in: Number of lag observations as input (X).
        n_out: Number of observations as output (y).
        dropnan: Boolean whether or not to drop rows with NaN values.
    Returns:
    --------
        Pandas DataFrame of series framed for supervised learning.
    """

    variables = data.columns.tolist()
    df = pd.DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)

    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('%s(t-%d)' % (j, i)) for j in variables]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('%s(t)' % (j)) for j in variables]
        else:
            names += [('%s(t+%d)' % (j, i)) for j in variables]
    # put it all together
    agg = pd.concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


class LightGBM_Modeling():
    """
    class using boosting method : XGBoost with Microsoft LightGBM implementation
    Parameters are set using hyperopt.

    """

    def __init__(self):
        self.params = {
            'colsample_bytree': 0.5641429122905353,
            'min_child_weight': 5,
            'num_leaves': 122,
            'metric': "mae"
        }

    def prepare_dataset(self, X_train, X_test, y_train, y_test):

        train_data = lgb.Dataset(X_train,
                                 feature_name=X_train.columns.tolist(),
                                 label=y_train)
        validation_data = lgb.Dataset(X_test,
                                      feature_name=X_test.columns.tolist(),
                                      label=y_test)

        return train_data, validation_data

    def fit(self,
            train_data,
            validation_data,
            early_stopping_rounds=5,
            num_round=200):

        bst = lgb.train(self.params,
                        train_data,
                        num_round,
                        valid_sets=[validation_data],
                        early_stopping_rounds=early_stopping_rounds)
        self.bst = bst
        return bst

    def evaluation(self,
                   y_train_true,
                   y_train_pred,
                   y_test_true,
                   y_test_pred,
                   method=["mae", "mape"],
                   verbose=False):
        """
        evaluate model by calculating MAE and MAPE by default

        Parameters:
        -----------
        y_train_true : numpy array or pd. Series
            y train true values

        y_train_pred : numpy array or pd. Series
            y train predicted values

        y_test_true : numpy array or pd. Series
            y test true values

        y_test_pred : numpy array or pd. Series
            y test predicted values

        method: list of str
            specify methods of evaluation, by default MAE and MAPE

        verbose: boolean
            True if wanting to print values

        Returns:
        --------
        eval: dict
            dictionary of evaluation metric as key and and score as value
        """

        eval = {}

        for m in method:

            score_train = eval_metric(y_train_true, y_train_pred, method=m)
            score_test = eval_metric(y_test_true, y_test_pred, method=m)

            if verbose:
                print("Train score (%s) : %.2f" % (m, score_train))
                print("Test score (%s) : %.2f" % (m, score_test))
            eval[m] = {"train": score_train, "test": score_test}

        return eval

    def feature_importances(self, ax, features):
        feature_imp = pd.DataFrame(sorted(
            zip(self.bst.feature_importance(), features)),
            columns=['Value', 'Feature'])
        df_sorted = feature_imp.sort_values(by="Value", ascending=False)

        sns.barplot(x="Value", y="Feature", data=df_sorted, ax=ax)

        return df_sorted

    def prediction_visualization(self, time, y, y_pred, ax=None):
        """
        visualize prediction 

        Parameters:
        -----------
        time: pd.Series or numpy array
            timepoint

        y: pd.Series or numpy array
            true y values

        y_pred : pd.Series or numpy array
            predicted y values

        ax : matplotlib axis
            Default value: None

        """

        sns.lineplot(x=time, y=y, ax=ax, color="blue", label="y_true")
        sns.lineplot(x=time, y=y_pred, ax=ax, color="red", label="y_pred")
