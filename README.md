# Correction dans le code

Dans le fichier model.py, on a changé la ligne 107. Vous pouvez retélécharger le dossier pour avoir la dernière version.

Avant:

```python
scoring = make_scorer(mean_absolute_error)
```

Après:

```python
scoring = make_scorer(mean_absolute_error, greater_is_better=False)
```

# Etude de cas

Ce repo contient les fichiers nécessaire pour les études de cas.

Préparé par l'équipe Capgemini Invent : P. Lee, P. Faure, G. Barmentlo, A. Bnioukil, A. Roffet

# Consignes

- Télécharger le code source soit avec git soit directement en cliquant sur le bouton téléchargement

```bash
git clone https://gitlab.com/patleemunseng/etude-cas-data-cs.git
```

- Aller dans le dossier etude-cas-data-cs-master
- Lancer requirements.txt avec pip

```bash
pip install -r requirements.txt
```

- Vous pouvez désormais travailler sur les études de cas

- Si vous avez téléchargé le repo avec la fonction git, vous pouvez le mettre à jour avec git pull

```bash
git pull
```

---

## Organisation du Projet

    ├── README.md   <- the top-level README for developers using this project.
    ├── requirements.txt   <- the requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    ├── Partie 1 - data_exploration.ipynb  <- 1st notebook for exploration
    ├── Partie 2 - modeling.ipynb   <- 2nd notebook for modeling
    ├── Partie 3 - complex_modeling.ipynb   <- 3rd notebook for complex modeling
    ├── utils.py   <- utiliy functions for data exploration
    ├── model.py   <- utility functions for modeling
    |── data
    │   ├── train_part_one.csv  <- train set for part 1 and 2
    │   ├── test_part_one.csv   <- test set for part 1 and 2
    │   ├── train_part_three.csv    <- train set for part 3
    │   ├── test_part_three.csv     <- test set for part 3
