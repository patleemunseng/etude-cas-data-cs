import pandas as pd
import numpy as np
import matplotlib.pylab as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder

sns.set_style("ticks")

def get_individual_sales_info(df, store_number=None, item_number=None):
    """
    get information on a specific store or item

    Parameters:
    -----------
    df : pd.DataFrame

    store_number : int
        selected store number

    item_number : int
        selected item

    Returns:
    --------
    df : pd.DataFrame

    """

    if store_number is None and item_number is None:
        print(
            'Veuillez choisir un magasin (store_number) et/ou un item (item_number)'
        )
        raise ValueError
    elif store_number is not None and item_number is not None:
        condition = np.logical_and(df["Store"] == store_number,
                                   df["Item"] == item_number)
    elif store_number is not None:
        condition = df["Store"] == store_number
    elif item_number is not None:
        condition = df["Item"] == item_number

    return df[condition]

def get_average_sales(df, item_number, store_number, time_unit, groupby=[], agg="mean"):
    """
    returns an aggregated dataframe depending at different granularity with an
    aggregation function

    df: pd.DataFrame

    item_number: int
        specific item_number

    store_number: int
        specific store_number

    time_unit: string
        time unit for aggregation. Can be "Date", "Week", "Month", "Year"

    agg: string
        aggregate function, can be median, mean, count, std etc. Default: mean

    """
    groupby_local = groupby.copy()

    if time_unit in ["Week", "Month", "Year"]:
        groupby_local += [time_unit]
        df_plot = df.copy().groupby(groupby_local).agg(agg)

    else:
        df_plot = df.copy()

    return df_plot


def plot_sales(df,
               store_number=None,
               item_number=None,
               ax=None,
               time_unit="Date",
               histogram=False,
               groupby=[],
               agg="mean"):
    """
    returns a matplotlib graph on Weekly_Sales

    Parameters:
    -----------

    df: pd.DataFrame

    store_number: int
        specific store_number

    item_number: int
        specific item_number

    ax: matplotlib.axes
        ax of the plot

    time_unit: string
        time unit for aggregation. Can be "Date", "Week", "Month", "Year"

    histogram: boolean
        True if plot histogram, line plot otherwise. Default: False

    agg: string
        aggregate function, can be median, mean, count, std etc. Default: mean


    """

    hue_number = 1

    df_temp = get_individual_sales_info(df,
                                        store_number=store_number,
                                        item_number=item_number)

    df_average = get_average_sales(df_temp,
                                   item_number,
                                   store_number,
                                   time_unit,
                                   groupby=groupby,
                                   agg=agg)

    # reset_index
    df_plot = df_average.reset_index()

    if store_number is not None and item_number is not None:
        target = df_plot[[time_unit, "Weekly_Sales"]]
        hue = None

        title = "Store number %d, Item number %d" % (store_number, item_number)

    elif item_number is not None:
        target = df_plot[[time_unit, "Item", "Store", "Weekly_Sales"]]
        hue = "Store"
        title = "Item number %d" % item_number

    elif store_number is not None:
        target = df_plot[[time_unit, "Item", "Store", "Weekly_Sales"]]
        hue = "Item"
        title = "Store number %d" % store_number

    if hue is not None:
        hue_number = len(target[hue].unique())

    if ax is None:
        fig, ax = plt.subplots(figsize=(8, 5))

    if histogram:
        if hue is not None:
            for item in target[hue].unique():
                ax = sns.distplot(target[target[hue] == item]["Weekly_Sales"],
                                  kde=False,
                                  label=item)
        else:
            ax = sns.distplot(
                target["Weekly_Sales"],
                kde=False,
            )
        ax.set_ylabel("Count")

    else:
        ax = sns.lineplot(x=time_unit,
                          y="Weekly_Sales",
                          hue=hue,
                          data=target,
                          legend="full",
                          palette=sns.color_palette("muted", hue_number))

    if agg == "mean" and not time_unit == "Date":
        ax.set_ylabel("Mean Weekly_Sales")

    if agg == "std" and not time_unit == "Date":
        ax.set_ylabel("Standard deviation Weekly_Sales")

    if agg == "median" and not time_unit == "Date":
        ax.set_ylabel("Median Weekly_Sales")

    ax.set_title(title)
    sns.despine()

    if hue is not None:
        if histogram:
            plt.legend(title=hue,
                       bbox_to_anchor=(1, 1),
                       loc=2,
                       borderaxespad=0.)
        else:
            plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.tight_layout()

def plot_sales_per_store_item(df,
                              item_number=None,
                              store_number=None,
                              ax=None,
                              agg="sum"):

    """
    returns matplotlib graph or a value on sales per store and/or item.

    Parameters:
    -----------
    df: pd.DataFrame

    store_number: int
        specific store_number

    item_number: int
        specific item_number

    ax: matplotlib.axes
        ax of the plot

    agg: string
        aggregate function, can be median, mean, std, sum etc. Default: sum
    """

    df_plot = get_individual_sales_info(df,
                                        store_number=store_number,
                                        item_number=item_number)

    if store_number is not None and item_number is not None:
        target = df_plot[["Weekly_Sales"]].agg(agg)

        title = "Store number %d, Item number %d" % (store_number, item_number)

    elif item_number is not None:
        target = df_plot[["Item", "Store", "Weekly_Sales"
                          ]].groupby(["Item", "Store"]).agg(agg).reset_index()

        x = "Store"
        title = "Item number %d" % item_number

    elif store_number is not None:
        target = df_plot[["Item", "Store", "Weekly_Sales"
                          ]].groupby(["Item", "Store"]).agg(agg).reset_index()

        x = "Item"
        title = "Store number %d" % store_number


    if store_number is not None and item_number is not None:
        print(agg + " Weekly Sales("+title +"): "+ str(target.values[0]))
    else:
        if ax is None:
            fig, ax = plt.subplots(figsize=(8, 5))

        ax = sns.barplot(
            x=x,
            y="Weekly_Sales",
            data=target,
        )
        ax.set_ylabel(agg + " Weekly_Sales")

        ax.set_title(title)
        sns.despine()

        plt.tight_layout()
